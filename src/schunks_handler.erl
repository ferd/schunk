-module(schunks_handler).
-export([init/3, handle/2, terminate/3]).

init(_Type, Req, _Opts) ->
    {ok, Req, undefined}.

handle(Req, State) ->
    {Reps, Req2} = cowboy_req:qs_val(<<"r">>, Req, <<"1">>),
    {ok, Req3} = reply(Req2, to_int(Reps)),
    {ok, Req3, State}.

terminate(_Reason, _Req, _State) ->
    ok.

reply(Req, Reps) ->
    io:format("Answering with ~p reps~n", [Reps]),
    {ok, Req2} = cowboy_req:chunked_reply(200, [{<<"content-type">>, <<"text/plain">>}], Req),
    loop(Req2, Reps).

loop(Req, 0) -> {ok, Req};
loop(Req, N) ->
    case cowboy_req:chunk("1", Req) of
        ok -> timer:sleep(500), loop(Req, N-1);
        {error, Reason} -> {error, Reason}
    end.

to_int(Val) ->
    try list_to_integer(binary_to_list(Val)) of
        N when N > 0 -> N;
        _ -> 1
    catch
        E:R ->
            io:format("ERROR: ~p -> ~p~n", [Val, {E,R}]),
            1
    end.
