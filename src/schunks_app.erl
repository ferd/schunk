-module(schunks_app).

-behaviour(application).

-export([start/0]).
%% Application callbacks
-export([start/2,
         stop/1]).


start() ->
    application:ensure_all_started(schunks).

%%%===================================================================
%%% Application callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application is started using
%% application:start/[1,2], and should start the processes of the
%% application. If the application is structured according to the OTP
%% design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%
%% @spec start(StartType, StartArgs) -> {ok, Pid} |
%%                                      {ok, Pid, State} |
%%                                      {error, Reason}
%%      StartType = normal | {takeover, Node} | {failover, Node}
%%      StartArgs = term()
%% @end
%%--------------------------------------------------------------------
start(_StartType, _StartArgs) ->
    Port = case os:getenv("PORT") of
        false -> application:get_env(schunks, http_listen_port, 8888);
        N -> list_to_integer(N)
    end,
    Dispatch = cowboy_router:compile([
            {'_', [{'_', schunks_handler, []}]}
        ]),
    cowboy:start_http(schunks_http_listener, 100, [{port, Port}],
        [{env, [{dispatch, Dispatch}]}]
    ),
    schunks_sup:start_link().

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application has stopped. It
%% is intended to be the opposite of Module:start/2 and should do
%% any necessary cleaning up. The return value is ignored.
%%
%% @spec stop(State) -> void()
%% @end
%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
